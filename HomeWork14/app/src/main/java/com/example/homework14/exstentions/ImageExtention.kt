package com.example.homework14.exstentions

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.homework14.R

fun ImageView.setImage(context: Context, url:String) {
    if(url == null) {
        setImageResource(R.drawable.ic_launcher_background)
    } else {
        Glide.with(context).load(url).placeholder(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background).into(this)
    }

}