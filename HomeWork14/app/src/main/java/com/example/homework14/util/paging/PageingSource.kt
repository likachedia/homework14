package com.example.homework14.util.paging

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.homework14.models.User
import com.example.homework14.network.RetrofitInstance
import kotlinx.coroutines.delay
import okio.IOException
import retrofit2.HttpException

class UserPagingSource(
) : PagingSource<Int, User.Data>() {

    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, User.Data> {
        return    try {
            delay(3000)
            val nextPageNumber = params.key ?: 1
            val response = RetrofitInstance.api.getCustomPost(nextPageNumber)
            val body = response.body()

            if(response.isSuccessful && body != null) {
                Log.i("resp", body.toString())
                 LoadResult.Page(
                    data = body.data!!,
                    prevKey =  if(body.totalPages!! > nextPageNumber!!) null else nextPageNumber - 1,
                    nextKey = if (body.data.isEmpty()) null else nextPageNumber + 1)
            } else {
               LoadResult.Error(Throwable())
            }

        } catch (exception: IOException) {
             LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, User.Data>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }
}