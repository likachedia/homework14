package com.example.homework14.util

import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.liveData
import com.example.homework14.base.BaseViewModel
import com.example.homework14.network.RetrofitInstance
import com.example.homework14.util.paging.UserPagingSource


class UserViewModel(private val repository: Repository): BaseViewModel(repository) {


    fun getPageSource() =
       Pager(config = PagingConfig(pageSize = 1), pagingSourceFactory = { UserPagingSource() })
            .liveData.cachedIn(viewModelScope)

}