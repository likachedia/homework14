package com.example.homework14.base

import androidx.lifecycle.ViewModel
import com.example.homework14.util.Repository

abstract class BaseViewModel(private val repository: Repository): ViewModel()