package com.example.homework14.network

import com.example.homework14.constants.Constants.Companion.BASE_URL
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitInstance {

    private val retrofit by lazy{
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(moshiConvertor())
            .build()
    }

    val api: SimpleApi by lazy {
        retrofit.create(SimpleApi::class.java)
    }

    private fun moshiConvertor() =
        MoshiConverterFactory.create(
            Moshi.Builder().addLast(KotlinJsonAdapterFactory())
                .build()
        )
}