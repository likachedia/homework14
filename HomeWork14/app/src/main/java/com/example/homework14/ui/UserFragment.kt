package com.example.homework14.ui


import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.homework14.adapters.UserLoadStateAdapter
import com.example.homework14.adapters.UsersAdapter
import com.example.homework14.databinding.FragmentUserBinding
import com.example.homework14.util.UserViewModel
import com.example.retrofit.BaseFragment

class UserFragment : BaseFragment<FragmentUserBinding, UserViewModel>(FragmentUserBinding::inflate) {
    override fun getViewModelClass() = UserViewModel::class.java
    private lateinit var adapter: UsersAdapter
    private lateinit var userRecycler: RecyclerView
    override fun start() {
        initRecycler()
    }

    private fun initRecycler() {
        userRecycler = binding.recycler
        userRecycler.layoutManager = LinearLayoutManager(requireContext() )
        adapter = UsersAdapter()
        binding.recycler.adapter = adapter.withLoadStateFooter(
            footer = UserLoadStateAdapter{adapter.retry()}
        )
        lifecycleScope.launchWhenCreated {
            viewModel.getPageSource().observe(viewLifecycleOwner, {
                adapter.submitData(lifecycle, it)
            })

        }

    }

}