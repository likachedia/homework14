package com.example.homework14.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.view.menu.MenuView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.homework14.databinding.ModelUsersBinding
import com.example.homework14.exstentions.setImage
import com.example.homework14.models.User

class UsersAdapter() :PagingDataAdapter<User.Data, UsersAdapter.UserViewHolder>(
    DiffCallback()
) {

  /*  init {
        this.withLoadStateFooter(

            footer = LocationLoadingStateAdapter(this)

        )
    }*/
    inner class UserViewHolder(private val binding: ModelUsersBinding):RecyclerView.ViewHolder(binding.root) {

        fun onBind(model:User.Data) {
            binding.userImg.setImage(itemView.context, model.avatar!!)
            binding.firstName.text = model.firstName
            binding.lastName.text = model.lastName
            binding.email.text = model.email
        }
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        getItem(position)?.let { holder.onBind(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(ModelUsersBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    class  DiffCallback: DiffUtil.ItemCallback<User.Data>() {
        override fun areItemsTheSame(oldItem: User.Data, newItem: User.Data): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: User.Data, newItem: User.Data): Boolean {
            return oldItem == newItem
        }
    }

}