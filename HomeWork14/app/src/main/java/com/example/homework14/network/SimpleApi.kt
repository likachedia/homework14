package com.example.homework14.network

import com.example.homework14.models.User
import retrofit2.Response
import retrofit2.http.*

interface SimpleApi {
    @GET("users")
    suspend fun getCustomPost(
        @Query("page")page:Int
    ): Response<User>
}